#!/bin/bash

set -e

rm -f *.csv
rm -rf 0.* *e-* 1* 2* 3* 4* 5* 6* 7* 8* 9*
rm -rf processor*
