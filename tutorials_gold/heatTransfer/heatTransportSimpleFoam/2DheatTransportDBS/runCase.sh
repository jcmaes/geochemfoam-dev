#!/bin/bash

set -e

rm -rf 0
cp -r 0_org 0


cp constant/polyMesh/blockMeshDictRun constant/polyMesh/blockMeshDict
cp constant/transportPropertiesDict constant/transportProperties

python3 << END
import os
nx=200 
ny=80

dx=5e-3/nx
kfval=180/dx/dx
os.system('sed -i "s/nx/'+str(nx)+'/g" constant/polyMesh/blockMeshDict')
os.system('sed -i "s/ny/'+str(ny)+'/g" constant/polyMesh/blockMeshDict')
os.system('sed -i "s/kfval/'+str(kfval)+'/g" constant/transportProperties')

END

blockMesh
setFields
smoothSolidSurface

simpleDBSFoam

cp [1-9]*/U 0/.
cp [1-9]*/phi 0/.

rm -rf [1-9]*

heatTransportSimpleFoam 

cp [1-9]*/T 0/.
rm -rf [1-9]*



