#!/bin/bash




# ###### DO NOT MAKE CHANGES FROM HERE ###################################



set -e

#cp -rf constant/polyMesh 0/.
python << END
import os
import h5py
import numpy as np

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

dimX = 240  # dimension of image in X
dimY = 80  # dimension of image in Y
dimZ = 8

print("count dir")
ndir = 0

for dir in os.listdir(os.getcwd()): 
  if (is_number(dir)):
    ndir +=1

os.system('rm -f magU.hdf5')

time = np.zeros(ndir)
C = np.zeros((dimX, dimY, dimZ, ndir))

tcount =0
for dir in os.listdir(os.getcwd()): 
  if (is_number(dir)):
    print(dir)
    time[tcount]=float(dir)
    x = np.zeros(dimX*dimY*dimZ)
    y = np.zeros(dimX*dimY*dimZ)
    z = np.zeros(dimX*dimY*dimZ)
    file = open(dir+"/cellCenters","r")
    Lines = file.readlines()
    count =0
    wbool=0
    for line in Lines:
      ls = line.strip()
      if (ls==")"):
        break
      if (wbool==1):
        x[count]=float(ls.split("(")[1].split(")")[0].split()[0]) 
        y[count]=float(ls.split("(")[1].split(")")[0].split()[1])
        z[count]=float(ls.split("(")[1].split(")")[0].split()[2])
        count +=1
      if (ls=="("):
        wbool=1

    file = open(dir+"/p","r")
    Lines = file.readlines()
    count =0
    wbool=0
    for line in Lines:
      ls = line.strip()
      if (ls==")"):
        break
      if (wbool==1):
        a = np.floor((x[count])/2.5e-6)
        b = np.floor((y[count])/2.5e-6)
        c = np.floor((z[count])/2.5e-6)
        C[a.astype(int), b.astype(int),c.astype(int),tcount] = float(ls)
        count +=1
      if (ls=="("):
        wbool=1
    tcount += 1



f = h5py.File("3DcalciteGrains.hdf5","w")

v = np.argsort(time)
time = time[v]
C=C[:,:,:,v]

f.create_dataset('time', data=time, dtype="float", compression="gzip")
f.create_dataset('C', data=C, dtype="float", compression="gzip")



f.close()




END


