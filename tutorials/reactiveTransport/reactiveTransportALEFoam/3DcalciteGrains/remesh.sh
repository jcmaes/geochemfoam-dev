#!/bin/bash

cp -r ../3DcalciteGrains ../temp
cd ../temp

./removeInternalFaces.sh

surfaceMeshTriangulate -patches '(movingWalls)' constant/triSurface/calciteGrains.stl
cp constant/triSurface/calciteGrains.stl ../3DcalciteGrains/constant/triSurface/calciteGrains.stl
rm -rf 0 0.* [1-9]*

./delete.sh
./initCase.sh
cp -r 0_org 0
cp -r constant/polyMesh 0/.
