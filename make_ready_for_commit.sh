source etc/bashrc

echo "###################### compile code ###########################################"
cd ThirdParty
./Allwmakie &> /dev/null
cd ..
./Allwmake &> /dev/null

echo "###############################################################################"

echo "###################### run test cases ########################################"

cd tutorials

echo "###################################################"

cd transport
echo "######## multiSpeciesTransportFoam ################"
cd multiSpeciesTransportFoam
echo "### 1D multiSpecies transport ####"
cd 1DmultiSpeciesTransport
./delete.sh &> /dev/null
./runCase.sh &> /dev/null
rm 0.*/uniform/prof*
cd ..
echo "##################################"
echo "#### Ketton transport #############"
cd Ketton
./delete.sh &> /dev/null
./initCase.sh &> /dev/null
./runCaseFlow.sh &> /dev/null
./runCaseTransport.sh &> /dev/null
./delete.sh &> /dev/null
cd ..
echo "################################"
cd ..
cd ..
echo "####################################################"

cd reactiveTransport
echo "######## reactiveTransportFoam #####################"
cd reactiveTransportFoam
echo "## aqueous equilibrium ####"
cd aqueousEquil
./delete.sh &> /dev/null
./runCase.sh &> /dev/null
rm 0.*/uniform/prof*
cd ..
echo "###############################"

echo "### linear retardation #########"
cd linearRetardation
./delete.sh &> /dev/null
./runCase.sh &> /dev/null
rm 0.*/uniform/prof*
cd ..
echo "#############################"

echo "### 2D Grainstome bimolecular #########"
cd 2DGrainstoneBimolecular
./delete.sh &> /dev/null
./initCase.sh &> /dev/null
./runCaseFlow.sh &> /dev/null
./runCaseReactiveTransport.sh &> /dev/null
rm 0.*/uniform/prof*
rm [1-9]*/uniform/prof*
rm *.out
cd ..
echo "#############################"

cd ..
echo "####### reactiveTransportALEFoam ######################"
cd reactiveTransportALEFoam
echo "### 3D calcite Grain #######"
cd 3DcalciteGrains
./delete.sh &> /dev/null
./runCase.sh &> /dev/null
cd ..
echo "###############################"

echo "#### 3D calcite post #########"
cd 3DcalcitePost
./delete.sh &> /dev/null
./initCaseLMR.sh &> /dev/null
./runCase0.sh &> /dev/null
./runCase.sh &> /dev/null
cd ..
echo "###############################"
cd ..
echo "####### reactiveTransportDBSFoam ####################"
cd reactiveTransportDBSFoam
echo "### 3D calcite Post #################################"
cd 3DcalcitePost
./delete.sh &> /dev/null
./initImageAMR.sh &> /dev/null
./runCaseAMR0.sh &> /dev/null
./runCaseAMR.sh &> /dev/null
rm [1-9]*/uniform/prof*
cd ..
echo "##############################"

cd ..
cd ..
echo "#####################################################"


cd multiphase
echo "############# interFoam #################################"
cd interFoam

echo "#### 2D throat #################"
cd 2Dthroat
./delete.sh &> /dev/null
./runCaseGC.sh &> /dev/null
processMaxVelocity &> /dev/null
rm 0.*/uniform/prof*
rm *e-05/uniform/prof*
cd ..
echo "################################"
cd ..
cd ..
echo "###################################################"

cd multiphase
echo "############# interFoam #################################"
cd interOSFoam

echo "#### 2D throat #################"
cd BubbleChannelCa10-5 
./delete.sh &> /dev/null
./runCase.sh &> /dev/null
rm 0.*/uniform/prof*
cd ..
echo "################################"
cd ..
cd ..
echo "###################################################"


cd multiphaseTransport
echo "####### interTransportFoam #########################"
cd interTransportFoam
echo "### 1D multiphase diffusion ###"
cd 1DmultiphaseDiff
./delete.sh &> /dev/null
./runCase.sh &> /dev/null
rm 0.*/uniform/prof*
rm 1*/uniform/prof*
rm 2*/uniform/prof*
cd ..
echo "###############################"

echo "### 1D multiphase transport ###"
cd 1DmultiphaseTransport
./delete.sh &> /dev/null
./runCaseCCST.sh &> /dev/null
rm 0.*/uniform/prof*
cd ..
echo "###############################"

echo "### 2D multiphase transport ###"
cd 2DthroatMultiphaseTransport
./delete.sh &> /dev/null
./runCase.sh &> /dev/null
rm -rf 0.*/uniform/prof* *e-05/uniform/prof*
cd ..
echo "################################"
cd ..
cd ..
echo "###################################################"

cd multiphaseTransport
echo "####### interTransportFoam #############################"
cd interTransferFoam
echo "### 1D transfer bewteen gas and water ###"
cd 1DtransferGasWater
./delete.sh &> /dev/null
./runCase.sh &> /dev/null
processSaturation &> /dev/null
rm 0.*/uniform/prof*
rm [1-9]*/uniform/prof*
cd ..

echo "###############################"

echo "### risingBubble #####"
cd risingBubble
./delete.sh &> /dev/null
./runCase.sh &> /dev/null
processSaturation &> /dev/null
rm -rf 0.*
cd ..

echo "###############################"
cd ..
cd ..
echo "####################################################"

cd multiphaseReactiveTransport
echo "######### interReactiveTransportFoam ################"
cd interReactiveTransportFoam
echo "## porespace reactive transport###"
cd poreSpaceReactiveTransport
./delete.sh &> /dev/null
./runCase.sh &> /dev/null
rm 0.*/uniform/prof*
cd ..
echo "#################################"
cd ..
cd ..
echo "#####################################################"

cd multiphaseReactiveTransport
echo "######### interReactiveTransportFoam ################"
cd interReactiveTransferFoam
echo "## 1D reactive transfer Gas water###"
cd 1DReactiveTransferGasWater
./delete.sh &> /dev/null
./runCase.sh &> /dev/null
rm [1-9]*/uniform/prof*
cd ..
echo "## 2D cavity reactive transfer Gas water###"
cd 2DCavity
./delete.sh &> /dev/null
./runCase.sh &> /dev/null
rm 0.5/uniform/prof*
cd ..
echo "#################################"
cd ..
cd ..
echo "#####################################################"

cd incompressible
echo "############# simpleDBSFoam #########################"
cd simpleDBSFoam
echo "############ est_subvol #############################"
cd est_subvol
./delete.sh &> /dev/null
./runCase.sh &> /dev/null
rm -rf 150/uniform/prof*
cd ..
echo "#####################################################"
cd ..
cd ..
echo "#####################################################"

cd heatTransfer 
echo "######## multiSpeciesTransportFoam ################"
cd heatTransportSimpleFoam
echo "### 2D heat transport ####"
cd 2DheatTransportDBS
./delete.sh &> /dev/null
./runCase.sh &> /dev/null
cd ..
echo "### Bentheimer ####"
cd Bentheimer
./delete.sh &> /dev/null
./initCaseLMR.sh &> /dev/null
./runCase.sh &> /dev/null
./delete.sh &> /dev/null
cd ..
echo "################################"
cd ..
cd ..
echo "####################################################"

cd ..


#######################################################################################i
### check if test cases differents ####
DIFF=$(diff -urq tutorials tutorials_gold) 
echo $DIFF
if [ "$DIFF" != "" ] 
then
    echo "Not ready for commit, some test cases don't match" > ready_for_commit.log

else

    echo "ready for commit" > ready_for_commit.log
    ### delete test cases ##############################################################
    cd tutorials

    #######################################################
    
    cd transport
    ### multiSpeciesTransportFoam ######################
    cd multiSpeciesTransportFoam
    ### 1D multiSpecies transport ###
    cd 1DmultiSpeciesTransport
    ./delete.sh
    cd ..
    ################################
    cd .. 
    cd ..

    #######################################################
    cd reactiveTransport
    ### reactiveTransportFoam ############################
    cd reactiveTransportFoam
    ### aqueous equilibirum ########
    cd aqueousEquil
    ./delete.sh
    cd ..
    ################################

    ### linear retardation #########
    cd linearRetardation
    ./delete.sh
    cd ..
    ################################
    ### 2D Grainstone bimolecular #########
    cd 2DGrainstoneBimolecular
    ./delete.sh
    cd ..
    ################################

    cd ..
    ### reactiveTransportALEFoam ########################
    cd reactiveTransportALEFoam
    ### 2D calcite post ############
    cd 3DcalciteGrains
    ./delete.sh
    cd ..
    ################################
    cd 3DcalcitePost
    ./delete.sh
    cd ..
    ###############################
    cd ..
    ### reactiveTransportDBSFOam ###################
    cd reactiveTransportDBSFoam
    #### 3D calcitePost ##########################
    cd 3DcalcitePost
    ./delete.sh
    cd ..
    ############################################### 
    cd ..
    cd ..
    ######################################################


    cd multiphase
    ### interFoam ######################################
    cd interFoam
    ### 2D throat ###################
    cd 2Dthroat
    ./delete.sh
    cd ..
    #################################

    cd ..
    cd ..

    ######################################################
    cd multiphase
    ### interFoam ######################################
    cd interOSFoam
    ### 2D throat ###################
    cd BubbleChannelCa10-5 
    ./delete.sh
    cd ..
    #################################

    cd ..
    cd ..

    ####################################################
    cd multiphaseTransport
    ### interTransportFoam ############################
    cd interTransportFoam
    ### 1D multiphase diffusion ####
    cd 1DmultiphaseDiff
    ./delete.sh
    cd ..
    ################################

    ### 1D multiphase transport #### 
    cd 1DmultiphaseTransport
    ./delete.sh
    cd ..
    ################################

    ### 2D multiphase transport #### 
    cd 2DthroatMultiphaseTransport
    ./delete.sh
    cd ..
    ################################
    cd ..
    cd ..
    ####################################################
    cd multiphaseTransport
    ### interTransportFoam ############################
    cd interTransferFoam
    ### 1D transfer Gas Water ####
    cd 1DtransferGasWater
    ./delete.sh
    cd ..
    ################################

    ### rising bubble #### 
    cd risingBubble
    ./delete.sh
    cd ..
    ################################
    cd ..
    cd ..
    ######################################################
    cd multiphaseReactiveTransport
    ### interReactiveTransportFoam #######################
    cd interReactiveTransportFoam
    ## pore space reactive transport##
    cd poreSpaceReactiveTransport
    ./delete.sh
    cd ..
    #################################
    cd ..
    cd ..
    ######################################################
    cd multiphaseReactiveTransport
    ### interReactiveTransportFoam #######################
    cd interReactiveTransferFoam
    ## pore space reactive transport##
    cd 1DReactiveTransferGasWater
    ./delete.sh
    cd ..
    #################################
    ## pore space reactive transport##
    cd 2DCavity
    ./delete.sh
    cd ..
    #################################

    cd ..
    cd ..
    ######################################################
    cd incompressible
    #####################################################
    cd simpleDBSFoam
    ################################################### 
    cd est_subvol
    ./delete.sh
    cd ..
    ####################################################
    cd ..
    cd ..

    ####################################################
    cd heatTransfer 
    ### multiSpeciesTransportFoam ######################
    cd heatTransportSimpleFoam
    ### 1D multiSpecies transport ###
    cd 2DheatTransportDBS
    ./delete.sh
    cd ..
    ################################
    cd ..
    cd ..

    ######################################################  
    cd ..

    #######################################################################################

    ### clean code #####i#################################################################
    ./Allwclean
    cd ThirdParty
    ./Allwclean
    rm -rf lib
    cd ..


    rm -rf lib
    rm -rf bin

fi
